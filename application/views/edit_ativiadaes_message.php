<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Desafio Web Developer - EUAX - Editar</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	.toggle-group{
		border: 1px solid #ced4da;
	}
	.toggle-handle{
		border: 1px solid #ced4da !important;
	}
	</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/EUAX/includes/bootstrap/css/bootstrap.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="/EUAX/includes/bootstrap/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" style="background-color: #191a40 !important;">
  <a class="navbar-brand" href="#"><img src="https://i0.wp.com/www.euax.com.br/wp-content/uploads/2019/02/Logo-Euax-Consulting-Cor.png?fit=100%2C67&ssl=1"  width="50" alt="Logotipo - Euax"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/EUAX/Welcome/">Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </div>
</nav>

<main role="main" class="container-fluid p-0 pr-sm-3 pl-sm-3">

  <div class="starter-template" style="margin-top:65px; ">
    <h2><span>Desafio Web Developer - Editar</span></h2>
    <hr>
    <div class="container">
		<form action="/EUAX/Welcome/editatividades/" method="post">
        <input type="hidden" name="id_atividade" value="<?php echo $id; ?>">
        <input type="hidden" name="id_projeto" value="<?php echo $id_projeto; ?>">
            <div class="form-group">
                <input type="text" id="defaultForm-email" class="form-control validate" required="required" name="nome" value="<?php echo $nome; ?>">
            </div>

            <div class="form-group">
                <input id="datepicker" placeholder="Data Inicio" name="data_inicio" required="required" value="<?php echo $data_inicio; ?>" />
            </div>

            <div class="form-group">
                <input id="datepickerFim" placeholder="Data Fim" name="data_fim" required="required" value="<?php echo $data_fim; ?>" />
            </div>

            <div class="form-group">
                <p>Finalizado:</p><input type="checkbox" name="finalizado" <?php echo $finalizado; ?> id="toggle-two">
            </div>

            
            <button type="submit" name="save" value="save" class="btn btn-success float-right">Editar Atividades</button>
        </form>
    </div>  
  </div>

</main><!-- /.container -->

<script>
	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',                
    	language: 'pt-BR',
		uiLibrary: 'bootstrap4'
	});
	$('#datepickerFim').datepicker({
		format: 'dd/mm/yyyy',                
    	language: 'pt-BR',
		uiLibrary: 'bootstrap4'
	});
</script>

<script>
  $(function() {
    $('#toggle-two').bootstrapToggle({
      on: 'SIM',
      off: 'NÃO'
    });
  })
</script>

</body>
</html>