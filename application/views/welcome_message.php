<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Desafio Web Developer - EUAX</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/EUAX/includes/bootstrap/css/bootstrap.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="/EUAX/includes/bootstrap/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" style="background-color: #191a40 !important;">
  <a class="navbar-brand" href="#"><img src="https://i0.wp.com/www.euax.com.br/wp-content/uploads/2019/02/Logo-Euax-Consulting-Cor.png?fit=100%2C67&ssl=1"  width="50" alt="Logotipo - Euax"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/EUAX/Welcome/">Home <span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </div>
</nav>

<main role="main" class="container-fluid p-0 pr-sm-3 pl-sm-3">

  <div class="starter-template" style="margin-top:65px; ">
	<h2><span>Desafio Web Developer</span> <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#modalLoginForm">Cadastrar Projeto</button></h2>
	<?php echo $prd_cadastrado; ?>
    <!-- MODAL CADASTRAR PROJETO -->
	<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h4 class="modal-title w-100 font-weight-bold">Novo Projeto</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				
				<form action="/EUAX/Welcome/savedata" method="post">
					<div class="modal-body mx-3">
						<div class="md-form mb-5">
							<input type="text" id="defaultForm-email" required="required" class="form-control validate" name="nome" placeholder="Nome do Projeto">
						</div>

						<div class="md-form mb-5">
							<input id="datepicker" placeholder="Data Inicio" required="required" name="data_inicio" />
						</div>

						<div class="md-form mb-5">
							<input id="datepickerFim" placeholder="Data Fim" required="required" name="data_fim" />
						</div>

					</div>
					<div class="modal-footer d-flex justify-content-center">
						<button type="submit" name="save" value="save" class="btn btn-success float-right">Cadastrar Projeto</button>
					</div>
				</form>
			</div>
		</div>
	</div>
		<table class="table">
		<thead class="thead-light">
			<tr>
			<th scope="col">#</th>
			<th scope="col">Nome Projeto</th>
			<th scope="col">Data Inicio</th>
			<th scope="col">Data Fim</th>
			<th scope="col">% Completo</th>
			<th scope="col">Atrasado</th>
			<th scope="col">Atividades</th>
			<th scope="col">Editar</th>
			<th scope="col">Excluir</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($query_projeto->result() as $row) { ?>
			<tr>			
			<th scope="row"><?php echo $row->id; ?></th>
			<td><?php echo $row->nome; ?></td>
			<td><?php echo datef($row->data_inicio, 'd/m/Y'); ?></td>
			<td><?php echo datef($row->data_fim, 'd/m/Y'); ?></td>
			<td><?php 
				$id = $row->id;

				$querySim = $this->db->query("SELECT * FROM atividades where id_projeto =$id AND finalizado = '1'");
				$rowSim = $querySim->num_rows();
			
				$queryC = $this->db->query("SELECT * FROM atividades where id_projeto =$id");
				$rowC = $queryC->num_rows();
			
				$Result = $rowSim / $rowC;
				$resultR = ( $Result * 100 );
				echo number_format($resultR,2,",",".").'%';
			?></td>
			<td>
			<?php //atrasado
				$queryA = $this->db->query("SELECT * FROM atividades where id_projeto =$id ORDER BY data_fim DESC");
				$rowA = $queryA->row();
				if ($row->data_fim > $rowA->data_fim){
					echo "SIM";
				}else{
					echo "NÃO";
				}; ?>
				
			</td>
			<td scope="row">
				<a href="/EUAX/Welcome/listatividades/<?php echo $id; ?>">
					<i class="far fa-plus-square"></i>
				</a>
			</td>
			<td scope="row">
				<a href="/EUAX/Welcome/editdata/<?php echo $id; ?>">
					<i class="far fa-edit"></i>
				</a>
			</td>

			<td scope="row">
				<a href="/EUAX/Welcome/deldata/<?php echo $id; ?>">
					<i class="far fa-trash-alt"></i>
				</a>
			</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
  </div>

</main><!-- /.container -->

<script>
	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',                
    	language: 'pt-BR',
		uiLibrary: 'bootstrap4'
	});
	$('#datepickerFim').datepicker({
		format: 'dd/mm/yyyy',                
    	language: 'pt-BR',
		uiLibrary: 'bootstrap4'
	});
</script>

</body>
</html>