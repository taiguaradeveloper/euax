<?php
//FUNÇÃO PARA FORMATAR A DATA
function datef($date = NULL, $format = 'd/m/Y')
{
    if (isset($date) && $format === 'Y-m-d') $date = implode('-', array_reverse(explode('/', $date)));
    if ($date !== '0000-00-00' && !empty($date)) {
        $d = date_create($date);
        $ndate = date_format($d, $format);
        unset($d);
    }

    return (isset($ndate)) ? $ndate : NULL;
}
