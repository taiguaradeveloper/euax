<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
	/*call CodeIgniter's default Constructor*/
	parent::__construct();
	
	/*load database libray manually*/
	$this->load->database();
	
	/*load Model*/
	$this->load->model('welcome_message');
	}

	//Welcome
	public function index()
	{
		//Tratamento alert
		if ($_GET['sucess'] == 'add'){
			$data['prd_cadastrado'] = '
			<div class="alert alert-success" role="alert">
				Atividade Cadastrada.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>';
		}else if ($_GET['sucess'] == 'edit'){
			$data['prd_cadastrado'] = '
			<div class="alert alert-warning" role="alert">
				Projeto Editado.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>';
		}else if($_GET['sucess'] == 'del'){
			$data['prd_cadastrado'] = '
			<div class="alert alert-danger" role="alert">
				Projeto Deletado.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>';
		}

		//select projetos
		$this->db->from('projetos');
		$this->db->order_by('id', 'DESC');
		$data['query_projeto']     = $this->db->get();

	  
		$this->load->view('welcome_message', $data);
		$this->load->model('welcome_message');
	}

	public function deldata($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('projetos');
		
		redirect('/?sucess=del', 'refresh');
	}

	public function editdata($id)
	{
		$id_projeto = $this->input->post('id_projeto');
		if ($id_projeto){
			$nome        = $this->input->post('nome');
			$data_inicio = $this->input->post('data_inicio');;
			$data_fim    = $this->input->post('data_fim');
			
			$data_inicio  = datef($data_inicio, 'Y-m-d');
			$data_fim     = datef($data_fim, 'Y-m-d');
			$data_update  = date('Y-m-d H:i:s');
			
			$data = array(
				'nome'             => $nome,
				'data_inicio'      => $data_inicio,
				'data_fim'         => $data_fim,
				'data_atualizacao' => $data_update
				);

			$this->db->where('id', $id_projeto);
			$this->db->update('projetos',$data);
			
			redirect('/?sucess=edit', 'refresh');
			
		}


		$query = $this->db->query("SELECT * FROM projetos where id =$id ");
		$row = $query->row();
		$data['id'] 		   = $row->id;
		$data['nome_projetos'] = $row->nome;
		$data['data_inicio']   = datef($row->data_inicio, 'd-m-Y');
		$data['data_fim']      = datef($row->data_fim, 'd-m-Y');
	  
		$this->load->view('edit_message', $data);
		$this->load->model('welcome_message');
	}
	
	public function savedata()
	{
		$save = $this->input->post('save');
		if ($save){
			
			$nome        = $this->input->post('nome');
			$data_inicio = $this->input->post('data_inicio');
			$data_fim    = $this->input->post('data_fim');

			$data_inicio  = datef($data_inicio, 'Y-m-d');
			$data_fim     = datef($data_fim, 'Y-m-d');
			$data_cadatro = date('Y-m-d H:i:s');
			
			$this->welcome_message->saverecords($nome,$data_inicio,$data_fim,$data_cadatro);	
			
			redirect('/?sucess=add', 'refresh');
		}

		$this->load->view('welcome_message', $data);
	
	}

	public function listatividades($id)
	{
		$data['id_projeto'] = $id;
		if ($_GET['sucess'] == 'add'){
			$data['prd_cadastrado'] = '
			<div class="alert alert-success" role="alert">
				Atividade Cadastrada.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>';
		}else if($_GET['sucess'] == 'del'){
			$data['prd_cadastrado'] = '
			<div class="alert alert-danger" role="alert">
				Atividade Deletado.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>';
		}

		$query = $this->db->query("SELECT * FROM projetos where id =$id ");
		$row = $query->row();
		$data['id'] 		   = $row->id;
		$data['nome_projetos'] = $row->nome;

		$data['id'] = $id;

		$this->db->from('atividades');
		$this->db->where('id_projeto', $id);
		$this->db->order_by('id', 'DESC');
		$data['query_projeto']     = $this->db->get();
		$data['data_hoje'] = date('Y-m-d');
		$query = $this->db->query("SELECT * FROM atividades WHERE id_projeto=$id");
	  
		if ($query->num_rows() == '0'){$data['displaynone'] = 'display:none';};
		$this->load->view('atividade_message', $data);
	}

	public function addatividades()
	{
		$save = $this->input->post('save');
		$id_projeto = $this->input->post('id');
		if ($save){
			
			$nome        = $this->input->post('nome');
			$data_inicio = $this->input->post('data_inicio');
			$data_fim    = $this->input->post('data_fim');

			$data_inicio  = datef($data_inicio, 'Y-m-d');
			$data_fim     = datef($data_fim, 'Y-m-d');
			$data_cadatro = date('Y-m-d H:i:s');
			
			$this->welcome_message->addatividades($id_projeto,$nome,$data_inicio,$data_fim,$data_cadatro);

			redirect("Welcome/listatividades/$id_projeto?sucess=add", 'refresh');
		
			//NAO PODE ISSO TEM Q REDIR - LISTATIVIDADE $this->load->view('atividade_message', $data);
		}
	}

	public function editatividades($id)
	{
		$id_atividade 	   = $this->input->post('id_atividade');
		$id_projeto        = $this->input->post('id_projeto');
		$finalizado = $this->input->post('finalizado');
		if($finalizado == 'on'){$finalizado = '1';}else{$finalizado='0';}

		if ($id_atividade){
			$nome        = $this->input->post('nome');
			$data_inicio = $this->input->post('data_inicio');;
			$data_fim    = $this->input->post('data_fim');
			
			$data_inicio  = datef($data_inicio, 'Y-m-d');
			$data_fim     = datef($data_fim, 'Y-m-d');
			
			$data = array(
				'nome'             => $nome,
				'data_inicio'      => $data_inicio,
				'data_fim'         => $data_fim,
				'finalizado'       => $finalizado
				);

			$this->db->where('id', $id_atividade);
			$this->db->update('atividades',$data);
			
			redirect("Welcome/listatividades/$id_projeto?sucess=edit", 'refresh');
			
		}

		$query = $this->db->query("SELECT * FROM atividades where id =$id ");
		$row = $query->row();
		$data['id'] 		 = $row->id;
		$data['id_projeto']  = $row->id_projeto;
		$data['nome']        = $row->nome;
		$data['data_inicio'] = datef($row->data_inicio, 'd-m-Y');
		$data['data_fim']    = datef($row->data_fim, 'd-m-Y');

		if($row->finalizado == '1'){$data['finalizado']  = 'checked';}else{$data['finalizado']  = '';}
		
	  
		$this->load->view('edit_ativiadaes_message', $data);
	}

	public function delatividadedata($id, $id_projeto)
	{
		$this->db->where('id', $id);
		$this->db->delete('atividades');
		
		redirect("Welcome/listatividades/$id_projeto/?sucess=del", 'refresh');
	}

	
}


